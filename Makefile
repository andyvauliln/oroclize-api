test:
	go test ./...

build:
	go build \
	  -race \
	  -o app

.PHONY: test build